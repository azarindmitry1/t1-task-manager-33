package ru.t1.azarin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.dto.request.user.UserChangePasswordRequest;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    public final static String NAME = "user-change-password";

    @NotNull
    public final static String DESCRIPTION = "Change user password.";

    @Override
    public void execute() {
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String newPassword = TerminalUtil.nextLine();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(getToken());
        request.setNewPassword(newPassword);
        getUserEndpoint().changePasswordResponse(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
