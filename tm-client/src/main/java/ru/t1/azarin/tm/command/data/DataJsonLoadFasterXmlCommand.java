package ru.t1.azarin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.request.data.DataJsonLoadFasterXmlRequest;

public final class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public final static String NAME = "data-load-json";

    @NotNull
    public final static String DESCRIPTION = "Load data from json file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD JSON]");
        @NotNull final DataJsonLoadFasterXmlRequest request = new DataJsonLoadFasterXmlRequest(getToken());
        getDomainEndpoint().jsonLoadFasterXmlResponse(request);
    }

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

}
