package ru.t1.azarin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.dto.request.project.ProjectShowByIndexRequest;
import ru.t1.azarin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.azarin.tm.model.Project;
import ru.t1.azarin.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public final static String NAME = "project-show-by-index";

    @NotNull
    public final static String DESCRIPTION = "Show project by index.";

    @Override
    public void execute() {
        System.out.println("[FIND PROJECT BY INDEX]");
        System.out.println("ENTER PROJECT INDEX:");
        @NotNull final Integer index = TerminalUtil.nextInteger() - 1;
        @NotNull final ProjectShowByIndexRequest request = new ProjectShowByIndexRequest(getToken());
        request.setIndex(index);
        @NotNull final Project project = getProjectEndpoint().showByIndexResponse(request).getProject();
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
