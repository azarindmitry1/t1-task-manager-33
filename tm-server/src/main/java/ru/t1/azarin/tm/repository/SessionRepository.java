package ru.t1.azarin.tm.repository;

import ru.t1.azarin.tm.api.repository.ISessionRepository;
import ru.t1.azarin.tm.model.Session;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

}
